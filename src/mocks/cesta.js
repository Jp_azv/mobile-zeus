import logo from '../../assets/logo.png';


export const cesta = {
  topo: {
    titulo: "Detalhe da cesta",
  },
  detalhes: {
    nome: "Ração",
    logo: logo,
    nomeLoja: "Vortex Petshop",
  },

  itens : {
    titulo: 'Itens da lista',
  }

}

export default cesta;