import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Topo from './componentes/Topo';
import Detalhes from './componentes/Detalhes';
import Lista from './componentes/Lista';


export default function Cesta({ topo, detalhes, itens }) {
  return <KeyboardAwareScrollView>
    <ScrollView>
      <Topo {...topo} />
      <View style={estilos.cesta}>
        <Detalhes {...detalhes} />
        <Lista {...itens} />
      </View>
    </ScrollView>
  </KeyboardAwareScrollView>
}

const estilos = StyleSheet.create({
  cesta: {
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
});
