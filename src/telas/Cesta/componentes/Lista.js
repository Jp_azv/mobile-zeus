import React, { useState, useEffect, Fragment } from 'react'
import { StyleSheet, View, TouchableOpacity, } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios'

import Texto from '../../../componentes/Texto';
import { cesta } from '../../../mocks/cesta';
import { DataTable } from 'react-native-paper'


function initialState() {
    return {
        name: '',
        price: 0,
        quantity: 0,
    }
}

export default function Lista() {

    const [item, setItem] = useState(initialState())
    const [todos, setTodos] = useState([])

    const get = () => {

        axios.get('http://localhost:3001/racao/get')
            .then(res => {
                setTodos(res.data)
            })
            .catch(err => {
                console.log(err)
            })
        console.log(todos)
    }

    useEffect(() => {
        get()
    }, [])

    const post = async () => {
        const valor = item;
        //valor.preço = parseFloat(valor.preço)
        console.log(valor)
        await axios.post('http://localhost:3001/racao/register', valor).then(() => {
            console.log('Deu certo')
            get()

        }, (err) => {
            console.log(err)
        })
    }

    const deletar = async (id) => {
        console.log(id)
        await axios.delete(`http://localhost:3001/racao/delete/${id}`).then(() => {
            window.location.reload()
        }, (err) => {
            console.log(err)
        })
    }



    return <View>
        <Texto>{cesta.itens.titulo}</Texto>
        <View style={estilos.inputs}>
            <Texto>Nome da ração</Texto>
            <TextInput style={estilos.textInput} placeholder='Digite o nome da ração' label='Nome da ração' onChangeText={(value) => {
                setItem({ ...item, name: value })
            }}></TextInput>
            <Texto>Preço da ração</Texto>
            <TextInput style={estilos.textInput} keyboardType='numeric' placeholder='Digite o preço da ração' label='Nome da ração' onChangeText={(value) => {
                setItem({ ...item, price: parseFloat(value) })
            }} ></TextInput>
            <Texto>Quantidade de pacotes</Texto>
            <TextInput style={estilos.textInput} keyboardType='numeric' placeholder='Digite a quantidade da ração' label='Nome da ração' onChangeText={(value) => {
                setItem({ ...item, quantity: parseInt(value) })
            }} ></TextInput>
            <View>
                <TouchableOpacity style={estilos.botaoRegistrar} onPress={post}>
                    <Texto style={estilos.textoBotao}>Registrar</Texto>
                </TouchableOpacity>
            </View>
        </View>
        <DataTable style={estilos.tabela}>
            <DataTable.Header>
                <DataTable.Title>Nome</DataTable.Title>
                <DataTable.Title>Preço</DataTable.Title>
                <DataTable.Title>Pacotes</DataTable.Title>
                <DataTable.Title style={estilos.acao}>Ação</DataTable.Title>
            </DataTable.Header>
            {todos.map((elemento) => {
                return (
                    <Fragment>
                        <DataTable.Row>
                            <DataTable.Cell>{elemento.name}</DataTable.Cell>
                            <DataTable.Cell >R${elemento.price},00</DataTable.Cell>
                            <DataTable.Cell >{elemento.quantity}</DataTable.Cell>
                            <DataTable.Cell ><button onClick={()=>deletar(elemento._id)}>Excluir</button></DataTable.Cell>
                        </DataTable.Row>
                    </Fragment>)
            })}
        </DataTable>
    </View>

}

const estilos = StyleSheet.create({
    ordem: {
        flexDirection: `row`,

    },

    inputs: {
        marginVertical: 10,
        height: 200,
        justifyContent: 'space-between'
    },

    acao: {
        marginLeft: 10
    },

    botaoRegistrar: {
        marginTop: 16,
        backgroundColor: "rgb(101, 47, 138)",
        paddingVertical: 16,
        borderRadius: 6,
    },
    textoBotao: {
        textAlign: "center",
        color: "#ffffff",
        fontSize: 16,
        lineHeight: 26,
        fontWeight: "bold",
    },
    
    textInput: {
        borderWidth: 1,
        borderColor: "rgb(101, 47, 138)",
        width: "60%"

    },


    tabela: {
        marginTop: 20
    }

})